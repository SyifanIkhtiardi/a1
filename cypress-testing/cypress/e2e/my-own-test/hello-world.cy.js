/// <reference types="cypress" />

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImR1bW15MjYiLCJfaWQiOiI2MzMyOWY2Y2ZlYjMyOTAwMDlkMjIzY2IiLCJuYW1lIjoiZHVtbXkiLCJpYXQiOjE2NjQyNjE5OTgsImV4cCI6MTY2OTQ0NTk5OH0'

describe('Basic Desktop Tests', () => {
        
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit('https://codedamn.com');
  });

  it('Login page looks good', ()=> {

    cy.contains('Sign in').click();
    cy.contains('Sign in to codedamn').should('exist');
    cy.get('[data-testid="google-oauth-btn"').should('exist');
    cy.get('[data-testid="github-oauth-btn"]').should('exist');
    cy.contains('Forgot your password?').should('exist');

  });

  it('Login page links work', ()=> {

    //1. Sign in page
    cy.contains('Sign in').click();
    //2. Password reset page
    cy.contains('Forgot your password?').click({force: true});
    //3. Verify your page url
    cy.url().should('include', '/password-reset')

    cy.url().then((value) => {
      cy.log('The real curren value is', value)
    })
    //4. Go back to Sign in page
    cy.go('back');

    cy.contains('Create one').click({force: true});
    cy.url().should('include', '/register');

  });

  it('Login should display correct error', () => {
   

    cy.contains('Sign in').click();

    cy.contains('Unable to authorize').should('not.exist');

    cy.contains('div', 'Email address / Username').find('input').first().type('Admin',{force:true})

    cy.get('[data-testid="password"]').type('admin',{force:true});

    cy.get('[data-testid="login"]').click({force: true});

    cy.contains('Unable to authorize').should('exist');
  })

  it('Login should work fine', () => {
   
    //TODO: Set this as localStorage for Authentication

    cy.contains('Sign in').click();


    cy.contains('div', 'Email address / Username').find('input').first().type('dummy26',{force:true})

    cy.get('[data-testid="password"]').type('dummy4765890',{force:true});

    cy.get('[data-testid="login"]').click({force: true});

    cy.url().should('include', '/dashboard');
  })
});